CIS 322 Project 6

Project: An ACP Brevet control calculator implemented with the use of frontend AJAX, Flask, MongoDB, and RestAPI.

Author: Austin Mohler

Contact: amohler@uoregon.edu

For the User: If the Control Distance of the Brevet Distance is greater than 120% , it will return the given starting date with no change of the open and close time.

If the Control Distance is greater than the Brevet Distance's i.e. (200, 300, 400, 600, 1000) and within 120% of the Brevet Distance, it will return the same starting and closing time.

If the Control Distance is negative, it will return the given starting date, and there will be no change to open and close.

If the Control Distance is equal to 0, the opening time will stay the same, but the closing time is 1 hour ahead of the starting time.

If the Control Distance is less than 60 and greater than 0, the closing time will use 20 km/hr, and there will be an extra hour added on for calculations.

If the Control Distance is equal to the Brevet Distance, the speed will be calculated using the control speed of the lower.

For Professor and GE: 
Let's just say this year didn't end well. I didn't put in enough time, I let personal matters get in my way, and I just didn't time manage correctly. Project 5 and 6 just didn't go well for me. I definitely needed to put in way more time. I'm not asking for forgiveness, I just wanted to be transparent. I appreciate that I got a regrade opportunity on Project 4, I really do. I should've managed my time better, and also not forgotten about my 13 hour travel day today. I'm extremely tired and I think it's just for the best to take an L and get minimum points. I'm sorry for wasting your time, I definitely haven't been the student you both deserve and I'm sorry about that. I plan on retaking, so I'll hopefully see you both around. I'm gonna put in effort to any remaining quizzes, and the final. I wanted to say thank you for all the help over the term, I wish I was a better student honestly. I hope I can be better next term about it. I had a lot happen to me this term and my mental health was in the gutter. That's not an excuse, but like I said I wanted to be transparent. I thought my api.py was a good file, I think I put together something good, I just couldn't get anything else to work. 

Thank you again,
-Austin